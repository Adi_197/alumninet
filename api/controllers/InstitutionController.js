module.exports = {
    show: function(req,response){
		var request = require('request');
		var Crawler = require('crawler');
        var returnJson = {metaData:{},data:{}};
        var flag = 0;
        var str;
        console.log(req.params.all());
        if(req.params.all().q)
		  str = req.params.all().q.trim();
		else if(req.params.all().name)
			str = req.params.all().name.trim();		
		if(req.params.all().form=='json')   flag = 1;
		var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
		var url = new RegExp(urlRegex, 'i');
		var c = new Crawler({
			maxConnections: 10,
			"callback":function(error,resp,done){
				var list = [];
				var returnJson = {};
				if(error){
					console.log(error);
				}
				else{
					var $ = resp.$;
					var heading = $("span[id*='alumni' i]");
					if(heading.text()){
						heading = heading.parent().next();
						while(heading.prop("tagName")!="H2"){
							if(heading.prop("tagName")=="UL"){
								heading.children().each(function(){
									arr = $(this).children("a");
									tag = $(arr[0]);
									name = tag.text();
									href = "https://en.wikipedia.org" + tag.attr('href');
									list.push({name:name,wiki:href});
								});
							}
							else{
								var child = heading.find("ul");
								if(child.length > 0){
									child.each(function(){
										$(this).children().each(function(){
											arr = $(this).children("a");
											tag = $(arr[0]);
											name = tag.text();
    									    href = "https://en.wikipedia.org" + tag.attr('href');
                                            list.push({name:name,wiki:href});
										})
									})
								}
							}
							heading = heading.next();
                        }
                        returnJson['data'] = "institution";
                        returnJson['name'] = str;
                        returnJson['alumni'] = list;
                    }
                    if(flag==0)
                        return response.view('institution-results',returnJson);
                    else
                        return response.json(returnJson);
				}
			}
		});
		if(url.test(str)){
			if(str[4]=='s')
				str = str.slice(0,4) + str.slice(5);
			c.queue(str);
	    }
		else{
			console.log(str);
			request.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyDvEVYmpwGuhRdIvCl5zTWP7QGLzAvACeM&cx=014953327142564599184:ba_n-qe95r4&q='+str+ ' institution',function(err,resp,body){
				body = JSON.parse(body);
				console.log(body);
				c.queue(body.items[0].link);
				}
			) 
		}
	}
}
