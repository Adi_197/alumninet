module.exports = {
    show: function(req,res){
        var type = req.param('search');
        if(!type) type='institution';
        var returnJson = {data:type};
        return res.view('dashboard',returnJson);
    },
    test:function(req,res){
        var returnJson = {data:"aa"};
        return res.view('institution-results',returnJson);
    }
}