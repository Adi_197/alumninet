module.exports = {

  profile: function(req, res) {
    const NLP = require('google-nlp');
    const request = require('request');

    var query = req.param('q');

    final_json = {
      name: '',
      img:'',
      organizations: [],
      links: {}
    }

    var groupBy = function(xs, key) {
      return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };

    function getImage(name){
      request.get('https://www.googleapis.com/customsearch/v1?key=%20AIzaSyBKU4eFxY4Igvw1EI7RP-nYl6SeLyOgHgA&cx=017993727050035632737:ji74ktpcpf8&searchType=image&q=' + name , function(err, resp, body) {
        body=JSON.parse(body);
        final_json.img=body.items[0].link;

        res.setHeader('Content-Type', 'application/json');
      //  console.log("sending")
        res.send(JSON.stringify(final_json));
      });
    }
    function getLinks(name) {
      request.get('https://www.googleapis.com/customsearch/v1?key=%20AIzaSyBKU4eFxY4Igvw1EI7RP-nYl6SeLyOgHgA&cx=017993727050035632737:ji74ktpcpf8&q=' + name + ' official profile', function(err, resp, body) {
        body = JSON.parse(body);
        data = body.items;
        dataGrouped = groupBy(data, 'displayLink');
        if (dataGrouped['twitter.com'])
          final_json.links.twitter = dataGrouped['twitter.com'][0].link;
        else
          final_json.links.twitter = ''

        if (dataGrouped['facebook.com'])
          final_json.links.facebook = dataGrouped['facebook.com'][0].link;
        else
          final_json.links.facebook = ''

        if (dataGrouped['linkedin.com'])
          final_json.links.linkedin = dataGrouped['linkedin.com'][0].link;
        else
          final_json.links.linkedin = ''

          getImage(name);

          //console.log("sent")
      });
    }

    function getEntities(text,name) {
      const apiKey = 'AIzaSyBKU4eFxY4Igvw1EI7RP-nYl6SeLyOgHgA';
      let nlp = new NLP(apiKey);
      //var text = req.param('q');

      output_json = {
        Organizations: [],
        success: false
      };
      nlp.analyzeEntities(text)
        .then(function(entities) {
          entities.entities.forEach(function(element, index) {
            if (element['type'] === 'ORGANIZATION') {
              output_json.Organizations.push(element);
            }
          })
          output_json.success = true;
          orgs_array = []
          output_json.Organizations.forEach(function(element, index) {
            org = {};
            org.name = element.name;
            org.wiki = element.metadata.wikipedia_url;
            orgs_array.push(org);
          })
          final_json.organizations=orgs_array;
          getLinks(name);
        })
        .catch(function(error) {
          console.log('Error:', error.message);
        })
    }

    function getWikiData(formattedName) {
      request.get('https://en.wikipedia.org/w/api.php?action=query&prop=revisions&format=json&rvprop=content&rvsection=1&titles=' + formattedName, function(err, response, body) {
        body = JSON.parse(body);
        text = body.query.pages[Object.keys(body.query.pages)[0]].revisions[0]['*'];
        text = text.replace(/<(?:.|\n)*?>/gm, '');
        text = text.replace(/{{(?:.|\n)*?}}/gm, '');
        text = text.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
        final_json.name=formattedName;
        //getLinks(formattedName);
        getEntities(text,formattedName);
      })
    }

    function validateURL(textval) {
      var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
      return urlregex.test(textval);
    }

    var gotURL = validateURL(query);
    var url;
    if (gotURL) {
      //console.log("Got URL. Skipping getting url");
      url = query;
    } else {
      //console.log("getting url");
      request('https://en.wikipedia.org/w/api.php?action=opensearch&search=' + query + '&limit=1&namespace=0&format=json', function(err, resp, body) {
        body = JSON.parse(body);
        final_json.links.wiki=body[3][0];
        url = body[2][0];
        getWikiData(body[1][0]);
      });
    }

  }
}
